<?php

namespace Nitra\ActionManagementBundle\Form\Type\ActionManagement;

use Admingenerated\NitraActionManagementBundle\Form\BaseActionManagementType\EditType as BaseEditType;
use Symfony\Component\Form\FormBuilderInterface;

class EditType extends BaseEditType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $Action = $builder->getData();
        
        if ($Action->getType() == 'product') {
            $builder->remove('category');
            $builder->remove('brand');
            $builder->remove('discount');
        } else {
            $builder->remove('products');
        }
    }
}