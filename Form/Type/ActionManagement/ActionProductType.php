<?php

namespace Nitra\ActionManagementBundle\Form\Type\ActionManagement;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ActionProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('productId', 'genemu_jqueryautocomplete_document', array(
            'route_name'            => 'Nitra_ActionManagementBundle_ActionManagement_Product_autocomplete',
            'class'                 => 'NitraProductBundle:Product',
            'label'                 => 'fields.products.product.label',
            'required'              => true,
        ));
        $builder->add('discountType', 'choice', array(
            'label'                 => 'fields.products.product.discountType.label',
            'choices'               => array(
                'percent'               => 'fields.products.product.discountType.choices.percent',
                'summ'                  => 'fields.products.product.discountType.choices.summ',
            ),
        ));
        $builder->add('discount', 'number', array(
            'label'                 => 'fields.products.product.discount.label',
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'            => 'Nitra\ActionManagementBundle\Document\ActionProduct',
            'translation_domain'    => 'NitraActionManagementBundle',
        ));
    }

    public function getName()
    {
        return 'product_action';
    }
}