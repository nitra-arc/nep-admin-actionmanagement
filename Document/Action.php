<?php

namespace Nitra\ActionManagementBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ODM\Document(collection="Actions")
 */
class Action
{
    use \Nitra\ProductBundle\Traits\AliasDocument;
    use \Nitra\StoreBundle\Traits\LocaleDocument;

    /**
     * @var string Identifier
     * @ODM\Id
     */
    protected $id;

    /**
     * @var string Name
     * @ODM\String
     * @Assert\NotBlank
     * @Assert\Length(max = 255)
     * @Gedmo\Translatable
     */
    protected $name;

    /**
     * @var \DateTime Date from which the active
     * @ODM\Date
     */
    protected $dateFrom;

    /**
     * @var \DateTime Date to which the active
     * @ODM\Date
     */
    protected $dateTo;

    /**
     * @var \Nitra\ProductBundle\Document\Badge Badge for action products
     * @ODM\ReferenceOne(targetDocument="Nitra\ProductBundle\Document\Badge")
     */
    protected $badge;

    /**
     * @var boolean True if all products has updated badgeSort field
     * @ODM\Boolean
     */
    protected $badgeUnset;

    /**
     * @var float Discount percent for products
     * @ODM\Float
     */
    protected $discount;

    /**
     * @var string Short action description on product page
     * @ODM\String
     * @Gedmo\Translatable
     */
    protected $productShortDescription;

    /**
     * @var string Short description on actions list
     * @ODM\String
     * @Gedmo\Translatable
     */
    protected $shortDescription;

    /**
     * @var string Full description on action page
     * @ODM\String
     * @Gedmo\Translatable
     */
    protected $description;

    /**
     * @var string Action type (total or product)
     * @ODM\String
     */
    protected $type;

    /**
     * @var ActionProduct[] Array of action products
     * @ODM\EmbedMany(targetDocument="ActionProduct", strategy="setArray")
     */
    protected $products;

    /**
     * @var \Nitra\ProductBundle\Document\Category Action category
     * @ODM\ReferenceOne(targetDocument="Nitra\ProductBundle\Document\Category")
     */
    protected $category;

    /**
     * @var \Nitra\ProductBundle\Document\Brand Action brand
     * @ODM\ReferenceOne(targetDocument="Nitra\ProductBundle\Document\Brand")
     */
    protected $brand;

    /**
     * @var integer Priority of action
     * @ODM\Int
     */
    protected $priority;
    
    /**
     * @var string Action image
     * @ODM\String
     */
    protected $image;

    /**
     * To string converter
     * @return string
     */
    public function __toString()
    {
        return (string) $this->name;
    }

    /**
     * Get id
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set dateFrom
     * @param date $dateFrom
     * @return self
     */
    public function setDateFrom($dateFrom)
    {
        $this->dateFrom = $dateFrom;
        return $this;
    }

    /**
     * Get dateFrom
     * @return date $dateFrom
     */
    public function getDateFrom()
    {
        return $this->dateFrom;
    }

    /**
     * Set dateTo
     * @param date $dateTo
     * @return self
     */
    public function setDateTo($dateTo)
    {
        $this->dateTo = $dateTo;
        return $this;
    }

    /**
     * Get dateTo
     * @return date $dateTo
     */
    public function getDateTo()
    {
        return $this->dateTo;
    }

    /**
     * Set badge
     * @param \Nitra\ProductBundle\Document\Badge $badge
     * @return self
     */
    public function setBadge($badge)
    {
        $this->badge = $badge;
        return $this;
    }

    /**
     * Get badge
     * @return \Nitra\ProductBundle\Document\Badge $badge
     */
    public function getBadge()
    {
        return $this->badge;
    }

    /**
     * Set discount
     * @param int $discount
     * @return self
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
        return $this;
    }

    /**
     * Get discount
     * @return int $discount
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set shortDescription
     * @param string $shortDescription
     * @return self
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;
        return $this;
    }

    /**
     * Get shortDescription
     * @return string $shortDescription
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * Set description
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set category
     * @param \Nitra\ProductBundle\Document\Category $category
     * @return self
     */
    public function setCategory($category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * Get category
     * @return \Nitra\ProductBundle\Document\Category $category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set type
     * @param string $type
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set product
     * @param ActionProduct $products
     * @return self
     */
    public function setProducts($products)
    {
        $this->products = $products;
        return $this;
    }

    /**
     * Get products
     * @return ActionProduct[] $products
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Set brand
     * @param \Nitra\ProductBundle\Document\Brand $brand
     * @return self
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
        return $this;
    }

    /**
     * Get brand
     * @return \Nitra\ProductBundle\Document\Brand $brand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set priority
     * @param int $priority
     * @return self
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
        return $this;
    }

    /**
     * Get priority
     * @return int $priority
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Set productShortDescription
     * @param string $productShortDescription
     * @return self
     */
    public function setProductShortDescription($productShortDescription)
    {
        $this->productShortDescription = $productShortDescription;
        return $this;
    }

    /**
     * Get productShortDescription
     * @return string $productShortDescription
     */
    public function getProductShortDescription()
    {
        return $this->productShortDescription;
    }

    /**
     * Set image
     * @param string $image
     * @return self
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * Get image
     * @return string $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Get badgeUnset
     * @return bool $badgeUnset флаг на бейдж акции
     */
    public function getBadgeUnset()
    {
        return $this->badgeUnset;
    }
    
    /**
     * Set badgeUnset
     * @param bool $badgeUnset
     * @return self
     */
    public function setBadgeUnset($badgeUnset)
    {
        $this->badgeUnset = $badgeUnset;
        return $this;
    }
}