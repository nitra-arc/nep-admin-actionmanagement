<?php

namespace Nitra\ActionManagementBundle\Controller\ActionManagement;

use Admingenerated\NitraActionManagementBundle\BaseActionManagementController\EditController as BaseEditController;

class EditController extends BaseEditController
{
    /** @return \Doctrine\Common\Cache\ApcCache */
    protected function getCache()           { return $this->container->get('cache_apc'); }

    /** @return \Doctrine\ODM\MongoDB\DocumentManager */
    protected function getDocumentManager() { return $this->container->get('doctrine_mongodb.odm.document_manager'); }

    /**
     * This method is here to make your life better, so overwrite  it
     *
     * @param \Symfony\Component\Form\Form $form the valid form
     * @param \Nitra\ActionManagementBundle\Document\Action $Action
     */
    public function preSave(\Symfony\Component\Form\Form $form, $Action)
    {
        $this->updateBadgeSorts($Action);
    }

    public function postSave(\Symfony\Component\Form\Form $form, $Action)
    {
        $session        = $this->getRequest()->getSession();
        $storeId        = $session->get('store_id');
        $mongoDbName    = $this->container->getParameter('mongo_database_name');
        $storeHost      = $this->container->get('doctrine_mongodb.odm.document_manager')->find('NitraStoreBundle:Store', $storeId)->getHost();
        $cache          = $this->getCache();
        $cache->delete($mongoDbName . '_nitra_actions_by_product_ids_' . $storeHost);
    }
    
    /**
     * @param \Nitra\ActionManagementBundle\Document\Action $action
     */
    protected function updateBadgeSorts($action)
    {
        $this->unsetBadgeSortsForOldProducts($action);
        
        if ($action->getBadge() && (($action->getDateTo() > new \DateTime()) || !$action->getDateTo())) {
            $qb             = $this->getBadgeSortsQueryBuilder($action->getProducts(), $action->getCategory(), $action->getBrand());
            $badge          = $action->getBadge();
            $badgeSortOrder = in_array('getSortOrder', get_class_methods($badge)) ? $badge->getSortOrder() : 0;

            foreach ($qb->getQuery()->execute() as $product) {
                $productAction = $this->getProductAction($product, $action);
                if ($productAction) {
                    $actionBadge            = $productAction->getBadge();
                    $actionBadgeSortOrder   = in_array('getSortOrder', get_class_methods($actionBadge)) ? $actionBadge->getSortOrder() : 0;
                    $product->setBadgeSorts(array(
                        'all'                           => -1,
                        $actionBadge->getIdentifier()   => $actionBadgeSortOrder,
                    ));
                } else {
                    $product->setBadgeSorts(array(
                        'all'                           => -1,
                        $badge->getIdentifier()         => $badgeSortOrder,
                    ));
                }
            }
        }
        $action->setBadgeUnset(false);
    }

    /**
     * @param \Nitra\ActionManagementBundle\Document\Action $action
     */
    protected function unsetBadgeSortsForOldProducts($action)
    {
        $uow = $this->getDocumentManager()->getUnitOfWork();

        $originalAction = $uow->getOriginalDocumentData($action);
        $products   = array_key_exists('products', $originalAction)
            ? $originalAction['products']
            : new \Doctrine\Common\Collections\ArrayCollection();
        $category   = array_key_exists('category', $originalAction)
            ? $originalAction['category']
            : null;
        $brand      = array_key_exists('brand', $originalAction)
            ? $originalAction['brand']
            : null;

        $qb = $this->getBadgeSortsQueryBuilder($products, $category, $brand);

        foreach ($qb->getQuery()->execute() as $product) {
            $productAction = $this->getProductAction($product, $action);
            $badgeSorts    = array('all' => -1);
            if ($productAction) {
                $badgeSorts[$productAction->getBadge()->getIdentifier()] = in_array('getSortOrder', get_class_methods($productAction->getBadge()))
                    ? $productAction->getBadge()->getSortOrder()
                    : 0;
            }
            $product->setBadgeSorts($badgeSorts);
        }
    }

    /**
     * @param \Nitra\ProductBundle\Document\Product               $product
     * @param \Nitra\ActionManagementBundle\Document\Action       $action
     * @return \Nitra\ActionManagementBundle\Document\Action|null
     */
    protected function getProductAction($product, $action)
    {
        $qb = $this->getDocumentManager()->createQueryBuilder('NitraActionManagementBundle:Action')
            ->field('_id')->notEqual($action->getId())
            ->field('badge.$id')->exists(true)
            ->field('priority')->lte($action->getPriority());
        $qb->addAnd($qb->expr()
            ->addOr($qb->expr()
                ->field('dateFrom')->lt(date_create("now"))
                ->field('dateTo')->gt(date_create("now"))
            )->addOr($qb->expr()
                ->field('dateFrom')->exists(false)
                ->field('dateTo')->gt(date_create("now"))
            )->addOr($qb->expr()
                ->field('dateFrom')->lt(date_create("now"))
                ->field('dateTo')->exists(false)
            )->addOr($qb->expr()
                ->field('dateFrom')->exists(false)
                ->field('dateTo')->exists(false)
            )
        );
        $expr = $qb->expr();
        $expr->addOr($qb->expr()
            ->field('products.productId')->equals($product->getId())
        );
        if ($product->getModel()->getBrand()) {
            $expr->addOr($qb->expr()
                ->field('brand.id')->equals($product->getModel()->getBrand()->getId())
            );
        }
        if ($product->getModel()->getCategory()) {
            $expr->addOr($qb->expr()
                ->field('category.id')->equals($product->getModel()->getCategory()->getId())
            );
        }
        $qb->addAnd($expr);

        return $qb
            ->sort('priority', 'asc')
            ->getQuery()->getSingleResult();
    }

    /**
     * @param \Doctrine\ODM\MongoDB\PersistentCollection    $products
     * @param \Nitra\ProductBundle\Document\Category|null   $category
     * @param \Nitra\ProductBundle\Document\Brand|null      $brand
     * @return \Doctrine\MongoDB\Query\Builder
     */
    protected function getBadgeSortsQueryBuilder($products, $category, $brand)
    {
        $dm     = $this->getDocumentManager();
        $qb     = $dm->createQueryBuilder('NitraProductBundle:Product')
            ->field('badge.$id')->exists(false);

        if ($products && count($products)) {
            $ids = array();
            foreach ($products as $product) {
                $ids[] = new \MongoId($product->getProductId());
            }
            $qb->field('_id')->in($ids);
        } elseif ($category || $brand) {
            $modelsQb = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Model')
                ->distinct('_id');

            if ($brand) {
                $modelsQb->field('brand.id')->equals($brand->getId());
            }
            if ($category) {
                $modelsQb->field('category.id')->equals($category->getId());
            }

            $modelsIds = $modelsQb->getQuery()->execute()->toArray();

            $qb->field('model.$id')->in($modelsIds);
        }

        return $qb;
    }
}