<?php

namespace Nitra\ActionManagementBundle\Controller\ActionManagement;

use Admingenerated\NitraActionManagementBundle\BaseActionManagementController\ActionsController as BaseActionsController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ActionsController extends BaseActionsController
{
    /** @return \Doctrine\Common\Cache\ApcCache */
    protected function getCache()           { return $this->container->get('cache_apc'); }

    /** @return \Doctrine\ODM\MongoDB\DocumentManager */
    protected function getDocumentManager() { return $this->container->get('doctrine_mongodb.odm.document_manager'); }

    /**
     *  @param \Nitra\ActionManagementBundle\Document\Action $Action
     */
    protected function executeObjectDelete(\Nitra\ActionManagementBundle\Document\Action $Action)
    {
        $qb = $this->getBadgeSortsQueryBuilder($Action->getProducts(), $Action->getCategory(), $Action->getBrand());

        foreach ($qb->getQuery()->execute() as $product) {
            $Action = $this->getProductAction($product, $Action);
            if ($Action) {
                $actionBadge            = $Action->getBadge();
                $actionBadgeSortOrder   = in_array('getSortOrder', get_class_methods($actionBadge)) ? $actionBadge->getSortOrder() : 0;
                $product->setBadgeSorts(array(
                    'all'                         => -1,
                    $actionBadge->getIdentifier() => $actionBadgeSortOrder,
                ));
            } else {
                $product->setBadgeSorts(array(
                    'all' => -1,
                ));
            }
        }

        parent::executeObjectDelete($Action);
    }

    /**
     * @param \Nitra\ProductBundle\Document\Product               $product
     * @param \Nitra\ActionManagementBundle\Document\Action       $Action
     * @return \Nitra\ActionManagementBundle\Document\Action|null
     */
    protected function getProductAction($product, $Action)
    {
        $qb = $this->getDocumentManager()->createQueryBuilder('NitraActionManagementBundle:Action')
            ->field('_id')->notEqual($Action->getId())
            ->field('badge.$id')->exists(true);
        $qb->addAnd($qb->expr()
            ->addOr($qb->expr()
                ->field('dateFrom')->lt(date_create("now"))
                ->field('dateTo')->gt(date_create("now"))
            )->addOr($qb->expr()
                ->field('dateFrom')->exists(false)
                ->field('dateTo')->gt(date_create("now"))
            )->addOr($qb->expr()
                ->field('dateFrom')->lt(date_create("now"))
                ->field('dateTo')->exists(false)
            )->addOr($qb->expr()
                ->field('dateFrom')->exists(false)
                ->field('dateTo')->exists(false)
            )
        );
        $expr = $qb->expr();
        $expr->addOr($qb->expr()
            ->field('products.productId')->equals($product->getId())
        );
        if ($product->getModel()->getBrand()) {
            $expr->addOr($qb->expr()
                ->field('brand.id')->equals($product->getModel()->getBrand()->getId())
            );
        }
        if ($product->getModel()->getCategory()) {
            $expr->addOr($qb->expr()
                ->field('category.id')->equals($product->getModel()->getCategory()->getId())
            );
        }
        $qb->addAnd($expr);

        return $qb
            ->sort('priority', 'asc')
            ->getQuery()->getSingleResult();
    }

    /**
     * @param \Doctrine\ODM\MongoDB\PersistentCollection    $products
     * @param \Nitra\ProductBundle\Document\Category|null   $category
     * @param \Nitra\ProductBundle\Document\Brand|null      $brand
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    protected function getBadgeSortsQueryBuilder($products, $category, $brand)
    {
        $dm     = $this->getDocumentManager();
        $qb     = $dm->createQueryBuilder('NitraProductBundle:Product')
            ->field('badge.$id')->exists(false);

        if ($products && count($products)) {
            $ids = array();
            foreach ($products as $product) {
                $ids[] = new \MongoId($product->getProductId());
            }
            $qb->field('_id')->in($ids);
        } elseif ($category || $brand) {
            $modelsQb = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Model')
                ->distinct('_id');

            if ($brand) {
                $modelsQb->field('brand.id')->equals($brand->getId());
            }
            if ($category) {
                $modelsQb->field('category.id')->equals($category->getId());
            }

            $modelsIds = $modelsQb->getQuery()->execute()->toArray();

            $qb->field('model.$id')->in($modelsIds);
        }

        return $qb;
    }

    /**
     * @Route("/action_product_autocomplete", name="Nitra_ActionManagementBundle_ActionManagement_Product_autocomplete")
     * @param Request $request
     * @return JsonResponse
     */
    public function actionManagementAutocompleteProductAction(Request $request)
    {
        $qb = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Product')
            ->field('isActive')->equals(true)
            ->limit(20);

        $term = $request->query->get('term');

        foreach (explode(' ', $term) as $key => $word) {
            if (trim($word)) {
                $Word = preg_quote($word);
                $qb ->addAnd($qb->expr()
                    ->field('fullNameForSearch')->equals(new \MongoRegex("/$Word/i"))
                );
            }
        }

        $result = array();
        foreach ($qb->getQuery()->execute() as $key => $product) {
            $result[$key] = $product->getFullNameForSearch();
        }

        return new JsonResponse($result);
    }
}