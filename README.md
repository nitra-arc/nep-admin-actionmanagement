# ActionManagementBundle

## Описание

данный бандл предназначен для:

* **создания и редактирования акций**

## Подключение

* **composer.json**

```json
{
    ...   
    "require": {
        ...
        "nitra/e-commerce-admin-actionmanagementbundle": "dev-master",
        ...
    }
    ...
}
```

* **app/AppKernel.php**

```php
<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    //...
    public function registerBundles()
    {
        //...
        $bundles = array(
            //...
            new Nitra\ActionManagementBundle\NitraActionManagementBundle(),
            //...
        );
        //...
        return $bundles;
    }
    //...
}
```

* **routing.yml**

```yml
nitra_action_management:
    resource:    "@NitraActionManagementBundle/Resources/config/routing.yml"
    prefix:      /{_locale}/
    defaults:
        _locale: ru
    requirements:
        _locale: en|ru
```

* **menu.yml**

```yml
actionManagement:
    translateDomain: 'menu'
    route: Nitra_ActionManagementBundle_ActionManagement_list
```